#!/bin/bash

NEWLOC=`curl -L "http://dynamic.telestream.net/downloads/download-desktop-presenter.asp?prodid=desktoppresenter" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | head -1 `

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi
